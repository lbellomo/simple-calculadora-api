# Simple Calculadora Api

## Instalación

Con python3.7:

`pip install -r requeriments.txt`

Luego de instalar, correr en el home del proyecto:

`uvicorn main:app`

Que arranca la api en: `http://127.0.0.1:8000`

## Documentación

La documentación se encuentra en [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)

![img doc](screenshot/doc.png)

La api soporta dos endpoints: un GET al `/` con un `item_id` para ver un resultado guardado y un POST al `/` con un payload de un json como:
`{"query": "37 + 5", "save": true}

Donde `query` es la cuenta como string y `save` un bool si se va a guardar.
Esto devuelve un json de la forma:

`{"out": 42, "id": 7}`

Donde `out` es el resultado y `id` es el id de la cuenta (para acceder con el otro endpoint). Si `save` es false entonces el `id` que devuelva siempre va a ser -1. En caso de estar mal formateada la `query` devuelve un error 400 con su respectivo mensaje.

Una documentación mas completa (y que permite probarla) en  [http://127.0.0.1:8000/docs](http://127.0.0.1:8000/docs)

## Testing

Para correr los test:

`pytest main.py`
