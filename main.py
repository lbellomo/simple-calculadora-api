# coding: utf-8

import shelve
from string import ascii_letters
from math import log

import pytest
from pydantic import BaseModel
from starlette.testclient import TestClient
from fastapi import FastAPI, HTTPException

db_name = "db"


def get_max_id() -> int:
    """Find the max id from the shelve. Return 0 if shelve is empty."""
    with shelve.open(db_name) as db:
        try:
            return int(max(db.keys())) + 1
        except ValueError:
            return 0


def save_result(out_id: int, out: float) -> None:
    """Save a result in the shelve."""
    with shelve.open(db_name) as db:
        db[str(out_id)] = out


def read_result(out_id: int) -> float:
    """Return the value of a item. Raise a 404 if item not found."""
    with shelve.open(db_name) as db:
        if out_id in db:
            return db[out_id]
        raise HTTPException(status_code=404, detail="Item not found")


class Item(BaseModel):
    query: str
    save: bool


app = FastAPI()


@app.get("/{item_id}")
async def read_item(item_id: str):
    return read_result(item_id)


@app.post("/")
async def process_item(item: Item):
    item_dict = item.dict()
    # map chars to python's like operations
    q = item_dict["query"].replace("–", "-").replace("^", "**").replace(",", ".")
    # a bit of input sanitation.
    q_temp = q.replace("log", "")
    if any(letter in q_temp for letter in ascii_letters):
        raise HTTPException(status_code=400, detail="Invalid query")

    try:
        out = eval(q)
    except BaseException as error:
        detail = error.args[0].capitalize()
        raise HTTPException(status_code=400, detail=detail)

    if item_dict["save"]:
        out_id = get_max_id()
        save_result(out_id, out)
    else:
        out_id = -1

    return {"out": out, "id": out_id}


client = TestClient(app)


@pytest.mark.parametrize(
    "query,status_code,out,msj_error",
    [
        ("40 + 2", 200, 42, ""),
        ("3 + 4 – log(23.2) ^ (2-1) * -1", 200, 10.144152278672264, ""),
        ("log(-1)", 400, None, "Math domain error"),
        ("42 / 0", 400, None, "Division by zero"),
        ("42 / ", 400, None, "Unexpected eof while parsing"),
        ('(42 + 42) / 2, print("evil thing")', 400, None, "Invalid query"),
    ],
)
def test_process_item(query, status_code, out, msj_error):
    response = client.post("/", json={"query": query, "save": "false"})
    assert response.status_code == status_code
    if not msj_error:
        assert response.json()["out"] == out
    else:
        assert response.json()["detail"] == msj_error
